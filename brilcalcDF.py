#!/cvmfs/cms-bril.cern.ch/brilconda3/bin/python3

import dataclasses
import functools
import logging
import os
import shlex
import timeit
import typing

import dateutil
import pandas

import opt
import param

logging.basicConfig(level=logging.DEBUG, format='%(levelname)s: %(message)s')
os.environ['NUMEXPR_MAX_THREADS'] = '16'

def timer(func:typing.Callable) -> typing.Callable:
    '''Timer decorator. Logs execution time for functions.'''
    # [Primer on Python Decorators](https://realpython.com/primer-on-python-decorators/)
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        t0 = timeit.default_timer()
        value = func(*args, **kwargs)
        t1 = timeit.default_timer()
        logging.debug(f'{func.__name__}(): {t1-t0:.6f} s')
        return value
    return wrapper

@dataclasses.dataclass
class Query:
    ''' Query `brilcalc` and return response as a `pandas.DataFrame`. See [https://cmslumi.web.cern.ch/#brilcalc] for more info.'''

    @classmethod
    def kwArgs(cls, subcommand:str, requiredOptions:str, options:typing.Dict[str,str], **kwargs) -> str:
        '''Map provided `kwargs` to the corresponding available `options` (dict which pairs `kw` to its corresponding `brilcalc` option/flag) for a given `subcommand`.'''
        query = f'brilcalc {subcommand} {requiredOptions}'
        for kw, val in kwargs.items():
            if not options.get(kw):
                continue
            elif isinstance(val, bool) and val:
                query += f'{options.get(kw)} '
            else:
                if kw in ['begin','start','from','end','finish','to'] and not isinstance(val, int):
                    val = cls.parseDatetime(val)
                query += f'{options.get(kw)} {shlex.quote(str(val))} '
        return query[:-1]

    @classmethod
    def lumi(cls, perFill:bool=False, expandBX:bool=False, **kwargs) -> typing.Tuple[pandas.Series, pandas.DataFrame]:
        '''Subcommand to show luminosity. By default, (i.e. without `--type` or `--normtag` options) the summary of the best known "online" luminosity is shown.'''
        options = {**opt.selector, **opt.filter, **opt.output, **opt.display, **opt.database, **opt.lumi}
        requiredOptions = f'--output-style csv --tssec '
        queryString = cls.kwArgs(subcommand='lumi', requiredOptions=requiredOptions, options=options, **kwargs)
        response = cls.brilcalc(queryString=queryString)
        header = cls.header(response=response, summ=kwargs.get('summ', False))
        summary = cls.summary(response=response, summ=kwargs.get('summ', False))
        df = cls.df(response=response, perFill=perFill, expandBX=expandBX)
        return (header.append(summary), df)

    @classmethod
    def beam(cls, perFill:bool=False, expandBX:bool=False, **kwargs) -> typing.Tuple[pandas.Series, pandas.DataFrame]:
        '''Subcommand to show beam information (timeinfo, single beam energy, beam intensities, ncolliding bunches) per lumi section.'''
        options = {**opt.selector, **opt.filter, **opt.output, **opt.display, **opt.database, **opt.beam}
        requiredOptions = f'--output-style csv --tssec '
        queryString = cls.kwArgs(subcommand='beam', requiredOptions=requiredOptions, options=options, **kwargs)
        response = cls.brilcalc(queryString=queryString)
        header = cls.header(response=response, summ=kwargs.get('summ', True))
        df = cls.df(response=response, perFill=perFill, expandBX=expandBX)
        return (header, df)

    @classmethod
    def trg(cls, **kwargs) -> pandas.DataFrame:
        '''Subcommand to show L1, HLT bit/path and prescale information per run/per lumi section. The hlt path shown by this subcommand is constrained to only those potentially relevant to luminosity.'''
        options = {**opt.output, **opt.database, **opt.trg}
        requiredOptions = f'--output-style csv '
        queryString = cls.kwArgs(subcommand='trg', requiredOptions=requiredOptions, options=options, **kwargs)
        response = cls.brilcalc(queryString=queryString)
        columns = response[0][1:].split(',') # drop leading '#' from header line and split by commas into a list of strings
        data = [line.split(',') for line in response if not line.startswith('#')] # skip lines starting with '#' (norm tag version, column headers, and summary)
        return pandas.DataFrame(data=data, columns=columns)

    @staticmethod
    @timer
    def brilcalc(queryString:str) -> typing.List[str]:
        '''Query brilcalc as a shell subprocess.'''
        logging.debug(f'{queryString}')
        return os.popen(queryString).read().splitlines()

    @staticmethod
    def header(response:typing.List[str], summ:bool) -> pandas.Series:
        '''Parse `data tag` and `norm tag` versions.'''
        header = response[0][1:].split(',')
        header = pandas.Series(index=[tag.split(':')[0].strip() for tag in header], data=[tag.split(':')[1].strip() for tag in header])
        if summ:
            logging.info(f'\n{header.to_string()}')
        return header

    @staticmethod
    def summary(response:typing.List[str], summ:bool) -> pandas.Series:
        '''Parse summary table.'''
        summary = {response[idx+1][1:]:response[idx+2][1:] for idx,line in enumerate(response) if line == '#Summary:'}
        summary = pandas.Series(index=list(summary.keys())[0].split(','), data=list(summary.values())[0].split(','))
        summary = pandas.to_numeric(arg=summary, errors='ignore')
        if summ:
            logging.info(f'\n{summary.to_string()}')
        return summary

    @staticmethod
    def splitRunFill(df:pandas.DataFrame) -> pandas.DataFrame:
        '''Split `df['run:fill']` and concatenate as two columns.'''
        runFill = df['run:fill'].str.split(':', expand=True).rename(columns={0:'run', 1:'fill'}) 
        return pandas.concat(objs=[df, runFill], axis='columns').drop(columns='run:fill')

    @staticmethod
    def splitLS(df:pandas.DataFrame) -> pandas.DataFrame:
        '''Split `df['ls']` and concatenate as two columns.'''
        LS = df['ls'].str.split(':', expand=True).rename(columns={0:'deliveredLS', 1:'recordedLS'})
        return pandas.concat(objs=[df, LS], axis='columns').drop(columns='ls')

    @staticmethod
    def datetime(df:pandas.DataFrame) -> pandas.DataFrame:
        '''Convert `df['time']` to `datetime64` and insert as new `df['dt']` column'''
        df['dt'] = pandas.to_datetime(df['time'], unit='s', utc=True)
        return df

    @staticmethod
    def numeric(df:pandas.DataFrame) -> pandas.DataFrame:
        '''Infer dtype and convert columns to int64 or float64 accordingly.'''
        return df.apply(pandas.to_numeric, errors='ignore')

    @staticmethod
    def parseDatetime(dt:str) -> str:
        '''Parse datetime string into the incredibly-specific format expected by `brilcalc`.'''
        try:
            return dateutil.parser.parse(dt).strftime('%m/%d/%y %H:%M:%S')
        except dateutil.parser._parser.ParserError:
            return dt

    @staticmethod
    def parseUnit(df:pandas.DataFrame) -> str:
        '''Parse units used for delivered/recorded lumi'''
        return df.columns.str.extract('^delivered\((.*)\)').dropna().squeeze()

    @classmethod
    def rearrangeCols(cls, df:pandas.DataFrame) -> pandas.DataFrame:
        '''Reorder columns.'''
        unit = cls.parseUnit(df)
        cols = pandas.Series(['dt','time','deliveredLS','recordedLS','run','fill',f'delivered({unit})',f'recorded({unit})',f'[bxidx bxdelivered({unit}) bxrecorded({unit})]','avgpu','intensity1','intensity2','[bxidx intensity1 intensity2]','ncollidingbx','E(GeV)','egev','beamstatus','source'])
        return df[cols[cols.isin(pandas.Series(df.columns))]]

    @classmethod
    @timer
    def aggPerFill(cls, df:pandas.DataFrame) -> pandas.DataFrame:
        '''Aggregate per-ls results into per-fill.'''
        unit = cls.parseUnit(df)
        aggDict = {'dt':'min', 'time':'min', 'deliveredLS':list, 'recordedLS':list, 'run':list, 'intensity1':'mean', 'intensity2':'mean', f'delivered({unit})':'sum', f'recorded({unit})':'sum'}
        aggDict = {k:v for k,v in aggDict.items() if k in df.columns.to_list()}
        return df.groupby('fill').agg(aggDict).reset_index()

    @staticmethod
    @timer
    def strSplit(col:pandas.Series) -> pandas.DataFrame:
        return col.str.strip('[]').str.split(expand=True).apply(pandas.to_numeric, errors='ignore')

    @staticmethod
    def mapBX(bcid:int, label:str, bxID:pandas.DataFrame, bxData:pandas.DataFrame) -> pandas.Series:
        bxData = bxData.where(cond=bxID.isin([bcid]).values) # [pandas DataFrame set value on boolean mask](https://stackoverflow.com/a/50158760)
        bxData = bxData.dropna(axis='columns', how='all') # drop all-`NaN` columns
        bxData = bxData.fillna(axis='columns', method='bfill') # fill `NaN` by propagating backwards valid values from last column
        return bxData.iloc[:,0].sort_index().rename(f'bx{bcid}_{label}') # return first column
        # return pandas.concat([bxData[col].dropna() for col in bxData]).sort_index().rename(f'bx{bcid}_{label}') # [Collapsing rows in a Pandas dataframe if all rows have only one value in their columns](https://stackoverflow.com/a/44320046)

    @classmethod
    @timer
    def flattenBX(cls, df:pandas.DataFrame) -> pandas.DataFrame:
        if df.columns.str.contains('bxidx bxdelivered').any():
            unit = cls.parseUnit(df)
            (col, label1, label2) = (f'[bxidx bxdelivered({unit}) bxrecorded({unit})]', f'delivered({unit})', f'recorded({unit})')
        elif df.columns.str.contains('bxidx intensity1').any():
            (col, label1, label2) = ('[bxidx intensity1 intensity2]', 'intensity1', 'intensity2')
        bxData = cls.strSplit(df[col])
        # bxData = df[col].str.strip('[]').str.split(expand=True).apply(pandas.to_numeric, errors='ignore')
        unique_bcid = pandas.Series(bxData.iloc[:,::3].values.flatten()).dropna().astype(int).sort_values().unique()
        bxData1 = pandas.concat([cls.mapBX(bcid=bcid, label=label1, bxID=bxData.iloc[:,::3], bxData=bxData.iloc[:,1::3]) for bcid in unique_bcid], axis=1)
        bxData2 = pandas.concat([cls.mapBX(bcid=bcid, label=label2, bxID=bxData.iloc[:,::3], bxData=bxData.iloc[:,2::3]) for bcid in unique_bcid], axis=1)
        df = pandas.concat([df.drop(labels=col, axis=1), bxData1, bxData2], axis=1)
        logging.info(f'BCID: {unique_bcid.tolist()}')
        return df

    @classmethod
    def df(cls, response:typing.List[str], perFill:bool=False, expandBX:bool=False) -> pandas.DataFrame:
        '''Parse `brilcalc` response into a `pandas.DataFrame`.'''
        columns = response[1][1:].split(',') # drop leading '#' from header line and split by commas into a list of strings
        data = [line.split(',') for line in response if not line.startswith('#')] # skip lines starting with '#' (norm tag version, column headers, and summary)
        df = pandas.DataFrame(data=data, columns=columns)
        if 'run:fill' in df.columns:
            df = df.pipe(cls.splitRunFill)
        if 'ls' in df.columns:
            df = df.pipe(cls.splitLS)
        df = df.pipe(cls.numeric)
        if 'time' in df.columns:
            df = df.pipe(cls.datetime)
        df = df.pipe(cls.rearrangeCols)
        if perFill:
            df = df.pipe(cls.aggPerFill)
        if expandBX:
            df = df.pipe(cls.flattenBX)
        # df = df.pipe(cls.rearrangeCols)
        return df


@dataclasses.dataclass
class LumiPOG(Query):
    '''Query `brilcalc` with LumiPOG recommendations. See [https://twiki.cern.ch/twiki/bin/view/CMS/TWikiLUM] for more info.'''

    unit:str = '/fb'
    lumipog:str = '/cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags'
    normtagBRIL:str = f'{lumipog}/normtag_BRIL.json'
    normtagPHYSICS:str = f'{lumipog}/normtag_PHYSICS.json'
    commdqm:str = '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification'

    legacyCert = {
        2015: f'{commdqm}/Collisions15/13TeV/Reprocessing/Cert_13TeV_16Dec2015ReReco_Collisions15_25ns_JSON_v2.txt',
        2016: f'{commdqm}/Collisions16/13TeV/Legacy_2016/Cert_271036-284044_13TeV_Legacy2016_Collisions16_JSON.txt',
        2017: f'{commdqm}/Collisions17/13TeV/Legacy_2017/Cert_294927-306462_13TeV_UL2017_Collisions17_GoldenJSON.txt',
        2018: f'{commdqm}/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
    }

    prelegacyCert = {
        2015: f'{commdqm}/Collisions15/13TeV/Reprocessing/Cert_13TeV_16Dec2015ReReco_Collisions15_25ns_JSON_v2.txt',
        2016: f'{commdqm}/Collisions16/13TeV/ReReco/Final/Cert_271036-284044_13TeV_ReReco_07Aug2017_Collisions16_JSON.txt',
        2017: f'{commdqm}/Collisions17/13TeV/ReReco/Cert_294927-306462_13TeV_EOY2017ReReco_Collisions17_JSON_v1.txt',
        2018: f'{commdqm}/Collisions18/13TeV/ReReco/Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt',
    }

    @classmethod
    def delivered(cls, year:int=2018) -> float:
        '''Determine delivered luminosity for the given `year`.'''
        (summary,df) = cls.lumi(beamstatus='STABLE BEAMS', machinemode='PROTPHYS', beamenergy=6500, unit=cls.unit, normtag=cls.normtagBRIL, begin=f'{year}-01-01 00:00:00', end=f'{year}-12-31 23:59:59', summ=False)
        intLumi = summary[f'totdelivered({cls.unit})']
        logging.info(f"total {year} delivered luminosity: {intLumi} {cls.unit}\n{80*'_'}")
        return intLumi

    @classmethod
    def legacy(cls, year:int=2018) -> float:
        '''Determine legacy recorded luminosity for the given `year`.'''
        (summary,df) = cls.lumi(unit=cls.unit, normtag=cls.normtagPHYSICS, inputfile=cls.legacyCert.get(year), summ=False)
        intLumi = summary[f'totrecorded({cls.unit})']
        logging.info(f"total {year} legacy recorded luminosity: {intLumi} {cls.unit}\n{80*'_'}")
        return intLumi

    @classmethod
    def prelegacy(cls, year:int=2018) -> float:
        '''Determine prelegacy recorded luminosity for the given `year`.'''
        (summary,df) = cls.lumi(unit=cls.unit, normtag=cls.normtagPHYSICS, inputfile=cls.prelegacyCert.get(year), summ=False)
        intLumi = summary[f'totrecorded({cls.unit})']
        logging.info(f"total {year} prelegacy recorded luminosity: {intLumi} {cls.unit}\n{80*'_'}")
        return intLumi

    @classmethod
    def run2(cls, method:str='delivered'):
        methodMap = {'delivered':cls.delivered, 'legacy':cls.legacy, 'prelegacy':cls.prelegacy}
        years = [2015,2016,2017,2018]
        intLumi = pandas.Series({str(year): methodMap[method](year) for year in years})
        intLumi['2015-2018'] = intLumi.sum()
        intLumi['2016-2018'] = intLumi[['2016','2017','2018']].sum()
        return intLumi.rename(f'{method} luminosity ({cls.unit})')


class Examples:

    def zeroBiasRun(run:int=325000) -> pandas.DataFrame:
        return Query.lumi(r=run, byls=True, minBiasXsec=80000, type='hfet', precision='2f', hltpath='HLT_ZeroBias_v6')[1]

    def pltFill(fill:int=6666):
        return Query.lumi(fill=fill, beamstatus='STABLE BEAMS', type='pltzero', byls=True)[1]

    def bxRun(run:int=314848):
        return Query.lumi(run=run, beamstatus='STABLE BEAMS', xing=True, xingTr=0.5, expandBX=True)[1]

    def beamPeriod(begin:str='2018-07-01', end:str='2018 jul 31') -> pandas.DataFrame:
       return Query.beam(begin=begin, end=end, beamstatus='stable beams', perFill=True)[1]

    def trgRun(run:int=325000) -> pandas.DataFrame:
        return Query.trg(run=run, prescale=True, hltpath='HLT_ZeroBias_v6')


@timer
def main():
    delivered = LumiPOG.run2('delivered')
    legacy = LumiPOG.run2('legacy')
    prelegacy = LumiPOG.run2('prelegacy')
    result = pandas.concat([delivered, legacy, prelegacy], axis=1)
    print(result.T)

if __name__ == "__main__":
    main()
