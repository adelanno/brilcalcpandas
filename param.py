#!/cvmfs/cms-bril.cern.ch/brilconda3/bin/python3

import enum
import typing

import pandas

# [brilcalc](https://cmslumi.web.cern.ch/#brilcalc)


class BeamStatus(enum.Enum):
    STABLE_BEAMS:str = 'STABLE BEAMS'
    STABLEBEAMS:str = 'STABLE BEAMS'
    STABLE:str = 'STABLE BEAMS'
    FLAT_TOP:str = 'FLAT TOP'
    FLATTOP:str = 'FLAT TOP'
    FLAT:str = 'FLAT TOP'
    ADJUST:str = 'ADJUST'
    SQUEEZE:str = 'SQUEEZE'


class MachineMode(enum.Enum):
    PROTPHYS:str = 'PROTPHYS'
    PROTON:str = 'PROTPHYS'
    PROTONPHYSICS:str = 'PROTPHYS'
    PROTON_PHYSICS:str = 'PROTPHYS'
    IONPHYS:str = 'IONPHYS'
    ION:str = 'IONPHYS'
    IONPHYSICS:str = 'IONPHYS'
    ION_PHYSICS:str = 'IONPHYS'
    PAPHYS:str = 'PAPHYS'
    PA:str = 'PAPHYS'
    PROTONNUCLEUS:str = 'PAPHYS'
    PROTON_NUCLEUS:str = 'PAPHYS'


class Connection(enum.Enum):
    OFFLINE:str = 'offline'
    ONLINE:str = 'online'
    ONLINEW:str = 'onlinew'
    DEV:str = 'dev'


class Luminometer(enum.Enum):
    BCM1F:str = 'bcm1f'
    BCM:str = 'bcm1f'
    BCM1FSI:str = 'bcm1fsi'
    BCMSI:str = 'bcm1fsi'
    DT:str = 'dt'
    HFET:str = 'hfet'
    HFOC:str = 'hfoc'
    PLTZERO:str = 'pltzero'
    PLT:str = 'pltzero'
    PLTSLINK:str = 'pltslink'
    PIXEL:str = 'pxl'
    PCC:str = 'pxl'
    PXL:str = 'pxl'
    RADMON:str = 'radmon'
    RAMSES:str = 'ramses'


class Unit(enum.Enum):
    KB:str = '/kb'
    B:str = '/b'
    MB:str = '/mb'
    UB:str = '/ub'
    NB:str = '/nb'
    PB:str = '/pb'
    FB:str = '/fb'
    AB:str = '/ab'
    KBHZ:str = f'hz{KB}'
    BHZ:str = f'hz{B}'
    MBHZ:str = f'hz{MB}'
    UBHZ:str = f'hz{UB}'
    NBHZ:str = f'hz{NB}'
    PBHZ:str = f'hz{PB}'
    FBHZ:str = f'hz{FB}'
    ABHZ:str = f'hz{AB}'
    E21:str = '1e21/cm2'
    E24:str = '1e24/cm2'
    E27:str = '1e27/cm2'
    E30:str = '1e30/cm2'
    E33:str = '1e33/cm2'
    E36:str = '1e36/cm2'
    E39:str = '1e39/cm2'
    E42:str = '1e42/cm2'
    E21S:str = f'{E21}s'
    E24S:str = f'{E24}s'
    E27S:str = f'{E27}s'
    E30S:str = f'{E30}s'
    E33S:str = f'{E33}s'
    E36S:str = f'{E36}s'
    E39S:str = f'{E39}s'
    E42S:str = f'{E42}s'


def units() -> typing.List[str]:
    b = [f'/{prefix}b' for prefix in ['k','','m','u','n','p','f','a']]
    hz = [f'hz{unit}' for unit in b]
    cm2 = [f'1e{power}/cm2' for power in [21,24,27,30,33,36,39,42]]
    cm2s = [f'{unit}s' for unit in cm2]
    return [*b, *hz, *cm2, *cm2s]

def minbiasCrossSections() -> pandas.DataFrame:
    # [Reference minbias cross section table](https://cmslumi.web.cern.ch/cmslumi/#--minBiasXsec-MINBIASXSEC)
    data = [(6500,'pp',80000),(2510,'pp',65000),(6370,'pbpb',7800000),(3500,'pp',71500),(4000,'pp',73000),(5000,'ppb',2061000),(8160,'ppb',2135000),(2720,'xexe',5650000)]
    return pandas.DataFrame(data=data, columns=['SingleBeamE(Gev)','AcceleratorMode','minBiasXsec(ub)'])
