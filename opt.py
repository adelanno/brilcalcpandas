#!/cvmfs/cms-bril.cern.ch/brilconda3/bin/python3

# [dict.fromkeys(iterable[, value]) # Create a new dictionary with keys from `iterable` and values set to `value`](https://docs.python.org/3/library/stdtypes.html#dict.fromkeys)

selector = {
    # [https://cmslumi.web.cern.ch/#common-command-options]
    **dict.fromkeys(['inputselection','input_selection','inputfile','input_file', 'i'], '-i'), # input lumi section selection file or string format `{run:[[startls,stopls],[startls,stopls],...]}` (run number must be quoted for file input; entire string must be quoted for string input)
    **dict.fromkeys(['begin','start','from','beginFill','beginRun','fromFill','fromRun','startFill','startRun'], '--begin'), # fill number, or run number, or UTC timestamp in 'MM/DD/YY HH:MM:SS' format
    **dict.fromkeys(['end','finish','to','endFill','endRun','finishFill','finishFill','toFill','toRun'], '--end'), # fill number, or run number, or UTC timestamp in 'MM/DD/YY HH:MM:SS' format
    **dict.fromkeys(['fill','f'], '-f'), # fill number
    **dict.fromkeys(['run','r'], '-r'), # run number
}

filter = {
    # [https://cmslumi.web.cern.ch/#common-command-options]
    **dict.fromkeys(['beamstatus','beam_status','b'], '-b'), # ['STABLE BEAMS', 'FLAT TOP', 'ADJUST', 'SQUEEZE']
    **dict.fromkeys(['amodetag','machinemode','machine_mode'], '--amodetag'), # ['PROTPHYS', 'IONPHYS', 'PAPHYS']
    **dict.fromkeys(['beamenergy','beam_energy'], '--beamenergy'), # beam energy in GeV
}

output = {
    # [https://cmslumi.web.cern.ch/#common-command-options]
    **dict.fromkeys(['outputfile','output_file','o'], '-o'), # output csv filename (redirects stdout)
    **dict.fromkeys(['outputstyle','output_style'], '--output-style'), # ['tab', 'csv', 'html']
}

display = {
    # [https://cmslumi.web.cern.ch/#common-command-options]
    **dict.fromkeys(['scalefactor','scale_factor','n'], '-n'), # apply the scalefactor 1/n to the output
    **dict.fromkeys(['cerntime','cern_time'], '--cerntime'), # switch to display result time field in CERN local time
    **dict.fromkeys(['tssec','ts_sec','ts','unixts'], '--tssec'), # switch to display result time field in unix timestamp
}

database= {
    # [https://cmslumi.web.cern.ch/#common-command-options]
    **dict.fromkeys(['connection','connect','c'], '-c'), # database service to connect to ['offline', 'online', 'onlinew', 'dev']
    **dict.fromkeys(['authfile','auth_file','authpath','auth_path','p'], '-p'), # database authentication file
}

lumi = {
    # [https://cmslumi.web.cern.ch/#lumi]
    **dict.fromkeys(['filedata','file_data'], '--filedata'), # input file or directory in hdf5 format. works only with `--byls` or `--byls --xing` and unit is fixed `hz/ub`.
    **dict.fromkeys(['byls','by_ls'], '--byls'), # show luminosity and average pileup by lumi section
    **dict.fromkeys(['xing','perbx','perBX'], '--xing'), # show luminosity by lumi section and per bunch crossing
    **dict.fromkeys(['xingId','xingid','xingID','xing_id','bcid','bxid'], '--xingId'), # select bunch(es) by id. input can be a single number or a comma separated list in file or string format
    **dict.fromkeys(['xingTr','xingtr','xing_tr','bxThr','bx_thr'], '--xingTr'), # select bunch(es) with luminosity above the specified fraction (float number between 0 and 1) of the max bx lumi
    **dict.fromkeys(['normtag','norm_tag'], '--normtag'), # apply calibration/correction function defined by a tag bound to a specific luminometer
    **dict.fromkeys(['unit','u'], '-u'), # show luminosity in the specified unit and scale the output value accordingly
    **dict.fromkeys(['type','luminometer'], '--type'), # show results from the selected luminometer ['hfoc','hfet','bcm1f','bcm1fsi','bcm1futca','pltzero','pltslink','dt','pxl','ramses','radmon'] (`--type` is required if `--without-correction` is specified)
    **dict.fromkeys(['withoutcorrection','without_correction','uncorrected','raw'], '--without-correction'), # show raw data taken by a specific luminometer (`--type` must be provided)
    **dict.fromkeys(['datatag','data_tag'], '--datatag'), # specific version of lumi and beam data. the initial `online` datatag is guaranteed to exist
    **dict.fromkeys(['precision','float','scientific','significant_digits'], '--precision'), # define the luminosity value output format and precision. the format string can be `[0-9]f` for fixed floating point format or `[0-9]e` for scientific notation.
    **dict.fromkeys(['hltpath','hlt_path'], '--hltpath'), # show nominal luminosity scaled down by HLT and L1 prescale factors also taking into account L1 bit masks. the hlt path may include a string fnmatch pattern (`*`, `?`, `[seq]`, `[!seq]` operators are supported).
    **dict.fromkeys(['ignoremask','ignore_mask'], '--ignore-mask'), # switch off the effect of L1 bit masks (used only for debugging)
    **dict.fromkeys(['withoutcheckjson','without_checkjson','nocheckjson'], '--without-checkjson'), # switch off cross-checking with the json selection
    **dict.fromkeys(['minbiasxsec','minBiasXsec','min_bias_xsec','minbias_crosssection'], '--minBiasXsec'), # specify minimum bias cross section (in ub) to use when calculating the average pileup column for the `--byls` output. this option should be used together with `--type` or `--normtag`.
}

beam = {
    # [https://cmslumi.web.cern.ch/#beam]
    **dict.fromkeys(['xing','perbx','perBX'], '--xing'), # show beam intensities per bunch crossing
}

trg = {
    # [https://cmslumi.web.cern.ch/#trg]
    **dict.fromkeys(['pathinfo','path_info'], '--pathinfo'), # show HLT path and its L1 seed expression as well as the logical relationship between the L1 bits in the expression. complicated L1seed logics with no simple scaling relationship between L1 bits are excluded.
    **dict.fromkeys(['prescale','pre_scale'], '--prescale'), # if no `--hltpath` filter is specified, show prescale index change during the given run. if `--hltpath` filter is given, show the real HLT and L1 prescale values of the selected run on the selected hltpath(s).
    **dict.fromkeys(['run','r'], '-r'), # run number
    **dict.fromkeys(['hltconfig','hlt_config'], '--hltconfig'), # numeric hltconfig id or string hltkey
    **dict.fromkeys(['hltpath','hlt_path'], '--hltpath'), # hlt path name or string pattern. the hlt path may include a string fnmatch pattern (`\*`, `?`, `\[seq]`, `\[!seq]` operators are supported).
    **dict.fromkeys(['ignoremask','ignore_mask'], '--ignore-mask'), # ignore the effect of masks on L1 bits on prescale values (used only for debugging)
}
